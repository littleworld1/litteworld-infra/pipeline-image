FROM debian:10.10-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get --yes upgrade && \
    apt-get install --no-install-recommends --yes \
      apt-transport-https \
      gcc-aarch64-linux-gnu \ 
      ca-certificates \
      gnupg2 \
      git \
      curl && \
    curl https://baltocdn.com/helm/signing.asc | apt-key add - && \
    echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list && \
    apt-get update && \
    apt-get install --no-install-recommends --yes \
      helm \
      jq \
      g++ \
      python3-dev \
      python3-pip \
      python3-wheel \
      python3-setuptools && \
    pip3 install \
      pylint-gitlab \
      flake8 \
      flake8-html \
      anybadge && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
